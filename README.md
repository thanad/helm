
## Simple kubernetes with Docker for Mac

```
kubectl create namespace nginx-ingress
helm install --name release-nginx stable/nginx-ingress --namespace=nginx-ingress
```
browse to page http://127.0.0.1 and http://hello.127.0.0.1.xip.io check result


```
helm install --name=release-hello helm-hello
```
browse to page http://127.0.0.1 and http://hello.127.0.0.1.xip.io check result
```
helm upgrade release-hello helm-hello --set ingress.enabled=true
helm upgrade release-hello helm-hello --set ingress.hosts=hello.127.0.0.1.xip.io
helm upgrade release-hello helm-hello --set ingress.hosts='{hi.127.0.0.1.xip.io, hello.127.0.0.1.xip.io}'
```


Delete all
```
helm delete --purge release-hello
helm delete --purge release-nginx
```